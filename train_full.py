import numpy as np
import argparse
import torch.optim as optim
from cnn_finetune import make_model
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split
import os
import torch.backends.cudnn as cudnn
from misc import ZaloDataset, get_fns_lbs, preprocess_with_augmentation, NB_CLASSES, normalize_05, save_checkpoint, AverageMeter, accuracy

########################################################################
# Parameters Setting
########################################################################
parser = argparse.ArgumentParser(description='cnn_finetune zalo landmark')
parser.add_argument('--batch-size', type=int, default=32, metavar='N',
                    help='input batch size for training (default: 32)')
parser.add_argument('--test-batch-size', type=int, default=64, metavar='N',
                    help='input batch size for testing (default: 64)')
parser.add_argument('--epochs', type=int, default=30, metavar='N',
                    help='number of epochs to train (default: 1)')
parser.add_argument('--lr', type=float, default=0.001, metavar='LR',
                    help='learning rate (default: 0.001)')
parser.add_argument('--weight-decay', type=float, default=0.0001, metavar='WD',
                    help='weight decay (default: 0.0001)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=500, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--model-name', type=str, default='se_resnext50_32x4d', metavar='M',
                    help='model name (default: se_resnext50_32x4d)')
parser.add_argument('--dropout-p', type=float, default=0.5, metavar='D',
                    help='Dropout probability (default: 0.5)')
parser.add_argument('--image-size', type=int, default=224, metavar='IS',
                    help='image size (default: 224)')
parser.add_argument('--train-dir', type=str, default='./results', metavar='TD',
                    help='Where the training (fine-tuned) checkpoint and logs will be saved to.')
parser.add_argument('--dataset-dir', type=str, default='./zalo_landmark', metavar='DD',
                    help='Where the dataset is saved to.')
parser.add_argument('--imbalance', type=int, default=0, metavar='IM',
                    help='Train with imbalance or not')

args = parser.parse_args()
use_cuda = not args.no_cuda and torch.cuda.is_available()
device = torch.device('cuda' if use_cuda else 'cpu')
cudnn.benchmark = True
best_prec3 = 0
print ('######################\n# Parameter settings\n######################')
print(args)

########################################################################
# Load pre-trained model
########################################################################
model = make_model(
    args.model_name,
    pretrained=True,
    num_classes=NB_CLASSES,
    dropout_p=args.dropout_p,
    input_size=(args.image_size, args.image_size)
)
model = model.to(device)
print(args.model_name)
########################################################################
# Load Dataset and split
########################################################################
json_file = os.path.join(args.dataset_dir, 'train_val2018.json')
data_dir  = os.path.join(args.dataset_dir, 'TrainVal/')

print('Loading data')
fns, lbs, cnt = get_fns_lbs(data_dir, json_file)

print('Total files in the original dataset: {}'.format(cnt))
print('Total files with > 0 byes: {}'.format(len(fns)))
print('Total files with zero bytes {}'.format(cnt - len(fns)))

train_fns, val_fns, train_lbs, val_lbs = train_test_split(fns, lbs, test_size=0.0, random_state=2)
print('Number of training imgs: {}'.format(len(train_fns)))
print('Number of validation imgs: {}'.format(len(val_fns)))

train_dataset = ZaloDataset(train_fns, train_lbs, transform=preprocess_with_augmentation(normalize_05, args.image_size))
#val_dataset = ZaloDataset(val_fns, val_lbs, transform=preprocess(normalize_05, args.image_size))

if (args.imbalance==1):
    class_sample_counts=np.unique(train_lbs, return_counts=True)[1]
    weights = 1. / torch.tensor(class_sample_counts, dtype=torch.float)
    samples_weights = weights[train_lbs]

    sampler = torch.utils.data.WeightedRandomSampler(
       weights=samples_weights,
       num_samples=len(samples_weights),
      replacement=True)

    train_loader = torch.utils.data.DataLoader(train_dataset, num_workers=1,batch_size=args.batch_size, sampler=sampler)
else:
    train_loader = DataLoader(dataset=train_dataset, num_workers=8,
                                  batch_size=args.batch_size,
                                  shuffle=True)

#test_loader = DataLoader(dataset=val_dataset, num_workers=1,
#                                   batch_size=args.batch_size,
#                                    shuffle=False)

########################################################################
# Optimization
########################################################################
criterion = nn.CrossEntropyLoss().to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.1)

########################################################################
# Training
########################################################################
def train(epoch):
    losses = AverageMeter()
    top1 = AverageMeter()
    top3= AverageMeter()
    model.train()

    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)

        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, target)

        # measure accuracy and record loss
        prec1, prec3 = accuracy(output, target, topk=(1, 3))
        losses.update(loss.item(), data.size(0))
        top1.update(prec1[0], data.size(0))
        top3.update(prec3[0], data.size(0))

        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            for param_group in optimizer.param_groups:
               print('Epoch: [{0}][{1}/{2}]\t'
                      'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                      'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                      'Prec@3 {top3.val:.3f} ({top3.avg:.3f})\t'
                      'Learning rate {lr:10f}'.format(
                       epoch, batch_idx, len(train_loader), loss=losses, top1=top1, top3=top3, lr=param_group['lr']))


########################################################################
# Validation
#######################################################################
def val():
    model.eval()
    losses = AverageMeter()
    top1 = AverageMeter()
    top3 = AverageMeter()

    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            loss= criterion(output, target)
            # measure accuracy and record loss
            prec1, prec3 = accuracy(output, target, topk=(1, 3))
            losses.update(loss.item(), data.size(0))
            top1.update(prec1[0], data.size(0))
            top3.update(prec3[0], data.size(0))

    print(' * Loss ({loss.avg:.4f}) Prec@1 {top1.avg:.3f} Prec@3 {top3.avg:.3f}'
          .format(loss=losses, top1=top1, top3=top3))
    return top3.avg

def main():
    global best_prec3
    print('######################\n# Start Training\n######################')
    for param in model._features.parameters():
        param.requires_grad = False

    for epoch in range(1, args.epochs+1):
        if epoch == 6:
            for param in model._features.parameters():
                param.requires_grad = True

        train(epoch)
        is_best=True
        if (is_best):
            save_checkpoint({
                'epoch': epoch ,
                'state_dict': model.state_dict(),
                'best_prec3': best_prec3,
                'optimizer': optimizer.state_dict(),
            }, is_best, filename=os.path.join(args.train_dir, args.model_name+'_best_full.pth.tar'))

        scheduler.step(epoch)

if __name__ == '__main__':
    main()
